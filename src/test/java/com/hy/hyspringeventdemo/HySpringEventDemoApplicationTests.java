package com.hy.hyspringeventdemo;

import com.hy.hyspringeventdemo.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class HySpringEventDemoApplicationTests {
    @Resource
    private UserService userService;

    @Test
    void contextLoads() {
        System.out.println(userService.getById(1));
    }

}
