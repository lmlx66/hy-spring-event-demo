package com.hy.hyspringeventdemo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hy.hyspringeventdemo.pojo.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hy
 * @since 2022-11-12
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
