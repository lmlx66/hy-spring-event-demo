package com.hy.hyspringeventdemo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
@MapperScan("com.hy.hyspringeventdemo.mapper")
public class HySpringEventDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HySpringEventDemoApplication.class, args);
    }

}
