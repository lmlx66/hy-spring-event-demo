package com.hy.hyspringeventdemo.controller;

import com.hy.hyspringeventdemo.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: 王富贵
 * @description:
 * @createTime: 2022/11/09 22:41
 */
@RestController
public class TranslationController {
    @Resource
    private UserService userService;
    @GetMapping("event1")
    public void testEvent() {
        userService.translationService();
    }
}
