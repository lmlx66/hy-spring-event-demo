package com.hy.hyspringeventdemo.envent;

import com.hy.hyspringeventdemo.pojo.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

/**
 * @author: 王富贵
 * @description: 事务监听事件
 * @createTime: 2022/11/09 22:19
 */
@Getter
@Setter
public class TranslationEvent extends ApplicationEvent {
    private User user;
    public TranslationEvent(User user) {
        super(user);
        this.user = user;
    }
}
