package com.hy.hyspringeventdemo.envent;

import com.hy.hyspringeventdemo.pojo.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

/**
 * @author: 王富贵
 * @description: 我的自定义事件
 * @createTime: 2022/11/21 18:37
 */
@Getter
@Setter
@ToString
public class MyEvent extends ApplicationEvent {
    // 事件内部传递的对象
    private User user;

    public MyEvent(User user) {
        super(user);
        //对象需要在构造函数中赋值
        this.user = user;
    }
}
