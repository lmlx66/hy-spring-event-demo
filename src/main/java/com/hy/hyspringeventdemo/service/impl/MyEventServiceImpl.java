package com.hy.hyspringeventdemo.service.impl;

import com.hy.hyspringeventdemo.envent.MyEvent;
import com.hy.hyspringeventdemo.pojo.User;
import com.hy.hyspringeventdemo.service.MyEventService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: 王富贵
 * @description: 普通事件测试服务类实现
 * @createTime: 2022/11/22 11:00
 */
@Service
public class MyEventServiceImpl implements MyEventService {

    // 注入发布器，ApplicationEventPublisher
    @Resource
    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void TestMyEventService() {
        //使用发布器的publishEvent，发布我们特定的事件
        applicationEventPublisher.publishEvent(new MyEvent(new User(12L,"王富贵")));
        System.out.println("MyEventService publish over");
    }
}
