package com.hy.hyspringeventdemo.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hy.hyspringeventdemo.envent.TranslationEvent;
import com.hy.hyspringeventdemo.mapper.UserMapper;
import com.hy.hyspringeventdemo.pojo.User;
import com.hy.hyspringeventdemo.service.UserService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author hy
 * @since 2022-11-12
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Resource
    private ApplicationEventPublisher applicationEventPublisher;

    @Resource
    private UserMapper userMapper;


    // 事务事件
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void translationService() {
        applicationEventPublisher.publishEvent(new TranslationEvent(new User(123L, "坏银")));
        userMapper.insert(new User(3L, "小明"));
        throw new RuntimeException("错");
    }
}
