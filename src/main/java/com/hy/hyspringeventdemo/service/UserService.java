package com.hy.hyspringeventdemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hy.hyspringeventdemo.pojo.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hy
 * @since 2022-11-12
 */
public interface UserService extends IService<User> {
    void translationService();
}
