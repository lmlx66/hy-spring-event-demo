package com.hy.hyspringeventdemo.service;

/**
 * @author: 王富贵
 * @description: 普通事件测试
 * @createTime: 2022年11月22日 11:00:02
 */
public interface MyEventService {
    void TestMyEventService();
}
