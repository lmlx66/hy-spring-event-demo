package com.hy.hyspringeventdemo.component;

import com.hy.hyspringeventdemo.envent.TranslationEvent;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * @author: 王富贵
 * @description: 事务监听器
 * @createTime: 2022/11/12 16:50
 */
@Component
public class TranslationEventListener {
    @Async
    @Transactional(propagation = Propagation.REQUIRES_NEW) // 不存在事务创建事务，存在事务创建一个新事务
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT, classes = TranslationEvent.class)
    public void onUserRegisterEvent(TranslationEvent translationEvent){
        System.out.println(translationEvent.getUser().getName());
    }
}
