package com.hy.hyspringeventdemo.component;

import com.hy.hyspringeventdemo.envent.MyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author: 王富贵
 * @description: 继承实现监听器
 * @createTime: 2022/11/21 19:13
 */
// 装配进容器
@Component
public class MyExtendsEventListener implements ApplicationListener<MyEvent> /*泛型就是监听的事件（消息）*/{
    //实现onApplicationEvent实现对监听的处理
    @Override
    @Async
    public void onApplicationEvent(MyEvent event) /*接收的事件（消息）实体，可以使用*/{
        System.out.println("监听到的事件MyEvent的用户名为："+event.getUser().getName());
    }
}
