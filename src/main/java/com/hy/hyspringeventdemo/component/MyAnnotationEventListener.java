package com.hy.hyspringeventdemo.component;

import com.hy.hyspringeventdemo.envent.MyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author: 王富贵
 * @description: 注解实现监听器
 * @createTime: 2022/11/22 10:42
 */
@Component
public class MyAnnotationEventListener {
    @EventListener(classes = MyEvent.class)
    @Async // 开启异步
    public void listenerMyEvent(MyEvent event  /*接收的事件（消息）实体，可以使用*/){
        System.out.println("监听到的事件MyEvent的用户名为："+event.getUser().getName());
    }
}
